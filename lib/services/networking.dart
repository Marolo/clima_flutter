import 'dart:convert';

import 'package:http/http.dart' as http;

class NetworkHelper {
  NetworkHelper();

  static const endPoint = "api.openweathermap.org";
  static const unEncodePath = "/data/2.5/weather";
  static const apiKey = "96b703602cdcb54cef482823e719a987";

  Future getData({double? lat, double? lon}) async {
    Map<String, dynamic> queryParams = {
      "appid": apiKey,
      "lat": "$lat",
      "lon": "$lon",
      "lang": "es",
      "units": "metric"
    };
    var url = Uri.https(endPoint, unEncodePath, queryParams);
    print(url);
    var response = await http.get(url);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print(response.statusCode);
      return null;
    }
  }

  Future getDataByName({String? name}) async {
    Map<String, dynamic> queryParams = {
      "appid": apiKey,
      "q": "$name",
      "lang": "es",
      "units": "metric"
    };
    var url = Uri.https(endPoint, unEncodePath, queryParams);
    print(url);
    var response = await http.get(url);

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print(response.statusCode);
      return null;
    }
  }
}
